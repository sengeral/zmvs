<html>
<head>
	<title>ZMVS Demo</title>
	<style>
		body {
			background-color: #f9f9f9;
			text-align: center;
			padding: 50px;
			font-family: "Lato","Helvetica Neue",Helvetica,Arial,sans-serif;
		}

		.box {
			background: #fefefe;
			border: 1px solid #e6e6e6;
			box-shadow: 0px 0px 15px rgba(0,0,0,0.1);
			border-radius: 3px;
			margin: 0px auto;
			max-width: 700px;
			padding: 50px;
		}

		.logo {
			margin-bottom: 40px;
		}
	</style>
</head>
<body>
	<main class="box">
		<img class="logo" src="logo.png" width="200" height="108"/>
		<h1>Hello world!</h1>
		<?php 
			if($_ENV["HOSTNAME"]) {
		?>
		<h3>My hostname is <?php echo $_ENV["HOSTNAME"]; ?></h3>
		<?php 
			}
		?>
	</main>
</body>
</html>