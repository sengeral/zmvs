# ZMVS Demo

Wir erstellen zwei Virtuelle Maschinen. Als Treiber brauchen wir auf macOS Virtualbox, da macOS den nativen Treiber nicht unterstützt. 

```sh
docker-machine create -d virtualbox vm1 # use --debug flag in case of errors
docker-machine create -d virtualbox vm2
docker-machine ls

NAME   ACTIVE   DRIVER       STATE     URL                         SWARM   DOCKER        ERRORS
rpi1    *        virtualbox   Running   tcp://192.168.99.100:2376           v18.01.0-ce   
rpi2    -        virtualbox   Running   tcp://192.168.99.101:2376           v18.01.0-ce   

```

Die IP Adressen (`192.168.99.100` and `192.168.99.101`) notieren, da wir diese später noch brauchen.

Mit einer der beiden Maschinen anfangen, indem wir 

```sh
eval $(docker-machine env rpi1)
```

eingeben.

Wir starten zwei instanzen der App auf der gewählen Maschine. 

```sh
docker run -d -p 8080:80 --name app1 alexmsenger/zmvs:1
docker run -d -p 8081:80 --name app2 alexmsenger/zmvs:1
```

Die Eingaben kurz auseinander genommen: Ganz hinten steht das Image der App, dass vorher auf ein Öffentliches Docker Image Repository ([hub.docker.com](hub.docker.com)) gepushed wurde. Das Image wird heruntergeladen, da es auf der Maschine noch nicht verfügbar ist.  a lightweight PHP script reading the ENV var for the hostname and displaying it. It will match the output of `docker ps`. In the above command the 

* `-d` lässt den Container im Hintergrund laufen (`--daemon`)
* `-p 8080:80` leitet den Port 8080 der Maschine auf Port 80 des Containers weiter.  
* `--name` gibt der Instanz einen Namen. 

Mit `docker ps` sehen wir nun welche Instanzen laufen. 

Das gleiche führen wir nun auf der anderen Instanz aus:

```sh
eval $(docker-machine env rpi2)

docker run -d -p 8080:80 --name app3 alexmsenger/zmvs:1
docker run -d -p 8081:80 --name app4 alexmsenger/zmvs:1
```

Nun gilt es den Load Balancer zu starten und auszuführen. Dazu nutzen wir ein blanko Nginx Image. 

```sh
docker run -d --name lb -p 80:80 nginx
```

> **NOTE**
> 
> Wir sind immer noch auf der zweiten Maschine (`rpi2`). Es macht keinen UNterschied auf welcher der beiden Maschinen wir den Load Balancer hinzu fügen. 

Wir loggen uns in den Container ein:

```sh
docker exec -it lb /bin/bash
```

und ändern die Standard Konfiguration unter `/etc/nginx/conf.d/default.conf`:

```sh 

echo "

upstream servers {
	server 192.168.99.100:8080;
	server 192.168.99.101:8080;
	server 192.168.99.100:8081;
	server 192.168.99.101:8081;
}

server {
	listen 80;

	location / {
		proxy_pass http://servers;
	}
}

" > /etc/nginx/conf.d/default.conf

```

Nun müssen wir den Container neu starten:

```sh
exit
docker restart lb
```

In unserem Browser sollten wir nun unter [http://192.168.2.100](http://192.168.2.100) unsere Webseite sehen. Wenn wir ein paar mal neu laden, sehen wir die verschiedenen Host Namen der Instanzen. 

## Failover

Um einen Failover / Switchover zu simulieren können wir eine Instanz stoppen. 

```sh
docker stop app3
```

Beim neuladen wir der Host Name nun nicht mehr in der Rotation auftauchen. Starten wir die Instanz wieder, wird sie automatisch wieder in die Verteilung mit aufgenommen. 






